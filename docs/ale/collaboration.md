# Collaboration

This page discusses Alcatel's collaboration solutions enabled by WarpDev.

## Rainbow <img alt="Deployed in WrpLab" src="../../static/img/wrplab.png" title="Solution deployed in Warpcom Lab" style="width: 25px">

> Rainbow is a cloud-based platform that enables your developers to integrate communications features (audio, video, messaging…) in your own applications or business processes.
>
> source: [openrainbow.com](https://www.openrainbow.com/solutions/)

### Features 

The development is possible with the following methods:

- SDK for Web
- SDK for Node.js
- SDK for Mobile (Android and iOS)
- REST APIs (administrative only)

In the following sections, we briefly discuss each SDK / API capability.

#### SDK for Web

The SDK for Web is an Angular JavaScript library that works from IE 11 to the latest version of Chrome, Firefox, Safari, and Edge. 
The library introduces functions that call Rainbow's REST API endpoints.  
For asynchronous functions, the SDK is based on [js Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise). 

It is distributed as an [NPM package](https://www.npmjs.com/package/rainbow-web-sdk) named ``rainbow-web-sdk``.

Even though the SDK for Web is an Angular based library it is compatible with JavaScript applications not based on the Angular framework like Backbone, React, Ember, Bootstrap or JQuery applications.

#### SDK for Node.js

The SDK for Node.js is a JavaScript package to connect a Node.js application to Rainbow. 
The library introduces functions that call Rainbow's REST API endpoints.  

It is distributed as an [NPM package](https://www.npmjs.com/package/rainbow-node-sdk) named ``rainbow-node-sdk``.

Regarding pre-requisites, the following is relevant:
 
- Node.js - from 6.x (with active support only for the last LTS version)
- NPM - from 3.6 (with active support only for the last LTS version)

This SDK is, by design, limited and it is neither possible to add contacts to a user's network nor accept an invitation from others users.

#### SDK for Android

The SDK for Android is a Java library to connect an Android application to Rainbow. It uses Android SDK version 16.

Regarding pre-requisites, the support for Android starts in version 4.1.

To develop audio video application with Rainbow SDK for Android you need a physical Android device to be configured and connected successfully to Android Studio.

#### SDK for iOS

The SDK is an Objective C library also embedding an audio/video stack from Google (WebRTC) to connect an iOS application to Rainbow. 

Regarding pre-requisites, the support for Android starts in version 10.0.

#### REST APIs

The REST APIs are a set of APIs based on HTTP methods which make more simple doing administrative tasks and provisioning. 

For all other types of integration with Rainbow (i.e., end-customer applications), the recommendation is to use an SDK (e.g., SDK for Web, Node.JS, IOS or Android).

Alcatel described the API with [OpenAPI](https://www.openapis.org/).

### Learning & Docs

Everything described above is in [hub.openrainbow.com](https://hub.openrainbow.com/#/documentation/doc/hub/getting-started).

In this page you'll also find:

- Guide with installation and some use cases;
- API documentation;
- Starter Kits - Github projects to demonstrate particular features;
- Cheat sheets.