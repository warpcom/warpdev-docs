# Alcatel-Lucent Enterprise

> ALE International, SAS, operating as [Alcatel-Lucent Enterprise](https://www.al-enterprise.com/), is a French company headquartered in Colombes, France, providing enterprise communication services.
> It was originally a division of Compagnie Générale d'Electricité, later known as Alcatel and then as Alcatel-Lucent. Alcatel-Lucent spun-off the division on 1 October 2014 to Chinese company China Huaxin. ALE International continued to use the Alcatel-Lucent brand name, which is now licensed from Nokia who purchased Alcatel-Lucent in 2015.
>At the time of the sale, ALE International had 2,700 employees, 500 000 clients, and a turnover of 700 million euros.

> source: [Wikipedia](https://en.wikipedia.org/wiki/Alcatel-Lucent_Enterprise)

## Alcatel and Warpcom

ALE has a particular connexion to Warpcom because our company was born, in 1989, as Alcatel - Comunicação de Empresa. Being a division of Alcatel business until 2002, ALE is one of our most significant installed bases.

Check [our history](https://warpcom.com/en/warpcom-history/) to understand how we've grown since then while keeping a close relation to ALE.

Warpcom is currently an Accredited Business Partner and an Enterprise Segment Specialist.

## Verticals

ALE develops its solutions by vertical, having a special edition of some of its products  according to: 

- Education
- Healthcare
- Hospitality
- Government
- Transportation

For their software solutions, it is also usual to present a product roadmap by vertical.

## Development

Regarding development, the focus goes into:

- [Rainbow](https://www.openrainbow.com/) - Collaboration service that connects people and things
- [OmniAccess Stellar Location-based Services (LBS)](https://www.al-enterprise.com/en/products/location-based-services/location-services-suite) -  Location-based services network solution and components
