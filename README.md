![Build Status](https://gitlab.com/pages/mkdocs/badges/master/build.svg)

---

WarpDev is the [Warpcom] branch responsible for the development of custom product
integrations.

This project aims to assemble the necessary information related to the APIs used 
by WarpDev.

Like WarpDev this repository is being continually updated with the new features 
published by vendors and capabilities developed by Warpcom.

These docs are available at https://warpdev.io!

These docs are currently outside the [WarpDev group](https://gitlab.com/warpcom/warpdev) due to GitLab's limitation to provide pages for subgroups. 
This project will be migrated as soon as [GitLab issue 30548](https://gitlab.com/gitlab-org/gitlab-ce/issues/30548) is solved.

---

## Ways to contribute

You can contribute to WarpDev Docs in these ways:

* Contribute to articles via the [public WarpDev docs repo](https://gitlab.com/warpcom/warpdev-docs)
* Report documentation bugs via [GitLab Issues](https://gitlab.com/warpcom/warpdev-docs/issues)

## Repository organization

The content in this documentation is grouped first by vendor, then by business unit.
Check the [How to read these docs?](https://warpdev.io/#how-to-read-these-docs) section.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](http://www.mkdocs.org/#installation) MkDocs
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)

---

## About this website

This website is built with [MkDocs] using a [theme] provided by 
[Read the Docs][read-the-docs].

It is hosted in [GitLab Pages][gitlab-pages] and build by [GitLab CI][ci].


[Warpcom]: https://warpcom.com
[mkdocs]: http://www.mkdocs.org
[theme]: https://github.com/snide/sphinx_rtd_theme
[read-the-docs]: https://readthedocs.org/
[gitlab-pages]: https://pages.gitlab.io
[ci]: https://about.gitlab.com/features/gitlab-ci-cd/
